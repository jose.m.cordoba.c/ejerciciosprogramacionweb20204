var obtenerPokemon = function(){
    let textoPokemon = document.getElementById("pkm").value;
    textoPokemon = textoPokemon.toLowerCase();
    obtenerDatosApi(textoPokemon);
}

var obtenerDatosApi = function ( pokemonName ) {
    var url = 'https://pokeapi.co/api/v2/pokemon/' + pokemonName;
    data = fetch(url)
        .then(response => response.json())
        .then(data => actualizarPokemon(data))
        .catch(function(error){
            alert('No existe ese pokemon');
        });
}

var actualizarPokemon = function ( data ) {
    let pokemon = {
        nombre : data.name,
        estatura : data.height,
        peso : data.weight,
        imagen : data.sprites.front_default,
        tipos : data.types,
        id : data.id,
    }
    actualizarNombre(pokemon.nombre);
    actualizarPeso(pokemon.peso);
    actualizarImagen(pokemon.imagen);
    actualizarEstatura(pokemon.estatura);
    actualizarListaTipos(pokemon.tipos);
}

var actualizarNombre = function( nombre ) {
    //Obtener el elemento del DOM
    elementoDOM = document.getElementById("nombre");
    //Generar el HTML interno
    innerHtml = "<strong>" + nombre + "</strong>";

    //Asignar el HTML interno al elemento
    elementoDOM.innerHTML = innerHtml;
}

var actualizarPeso = function( peso ) {
    //Obtener el elemento del DOM
    elementoDOM = document.getElementById("peso");
    //Generar el HTML interno
    innerHtml = "Peso: " + peso + "Kg";

    //Asignar el HTML interno al elemento
    elementoDOM.innerHTML = innerHtml;
}

var actualizarListaTipos = function ( listaTipos ){
    elementoDOM = document.getElementById("listaTipos");
    //Generar el HTML interno
    innerHtml = '<ul>' ;
    // ciclo del for    recorriendo el objeto json
    for (let index = 0; index < listaTipos.length; index++) {
        const element = listaTipos[index];
        innerHtml += '<li>'+ element.type.name +'<li>';   
    }
    innerHtml += '<ul>' ;
    //Asignar el HTML interno al elemento
    elementoDOM.innerHTML = innerHtml;
}

var actualizarEstatura = function( estatura ) {
    //Obtener el elemento del DOM
    elementoDOM = document.getElementById("estatura");
    //Generar el HTML interno
    innerHtml = "Estatura: " + estatura + " cm";

    //Asignar el HTML interno al elemento//
    elementoDOM.innerHTML = innerHtml;
}

var actualizarImagen = function( imagen ) {
    //Obtener el elemento del DOM
    elementoDOM = document.getElementById("sprite");
    //Generar el HTML interno
    innerHtml = '<img src="'+ imagen +'" alt="Sprite Ditto">';

    //Asignar el HTML interno al elemento
    elementoDOM.innerHTML = innerHtml;
}